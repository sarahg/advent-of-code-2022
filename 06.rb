module Day06
  class << self
    def part_one(input)
      input[0].chars.each_cons(4).with_index do |chars, i|
        next if chars.uniq.size < 4
        return i + 4
        break
      end
    end

    def part_two(input)
      input[0].chars.each_cons(14).with_index do |chars, i|
        next if chars.uniq.size < 14
        return i + 14
        break
      end
    end

  end
end
