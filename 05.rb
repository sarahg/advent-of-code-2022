module Day05
  class << self

    def getStacks()
      stacks = {
        1 => %w[W D G B H R V],
        2 => %w[J N G C R F],
        3 => %w[L S F H D N J],
        4 => %w[J D S V],
        5 => %w[S H D R Q W N V],
        6 => %w[P G H C M],
        7 => %w[F J B G L Z H C],
        8 => %w[S J R],
        9 => %w[L G S R B N V M],
      }
    end

    def part_one(input)
      stacks = getStacks()
      input.each do | move |
        command = move.split
        command[1].to_i.times do
          crate = stacks[command[3].to_i].pop
          stacks[command[5].to_i] << crate
        end
      end
      return stacks.values.map(&:last).join
    end

    def part_two(input)
      stacks = getStacks()
      input.each do | line |
        command = line.split
        crates = []
        command[1].to_i.times do
          crate = stacks[command[3].to_i].pop
          crates.unshift(crate)
        end
        stacks[command[5].to_i] += crates
      end
      return stacks.values.map(&:last).join
    end

  end
end
