module Day04
  class << self

    def part_one(input)
      fullOverlaps = 0

      input.each do | pair |
        elves = Array.new()
        pair.split(',').each do | elf |
          elves.push(([*elf.split('-')[0]..elf.split('-')[1]]))
        end

        if (elves[0] - elves[1]).empty? || (elves[1] - elves[0]).empty?
          fullOverlaps += 1
        end
      end

      return fullOverlaps
    end

    def part_two(input)
      kindaOverlaps = 0

      input.each do | pair |
        elves = Array.new()
        pair.split(',').each do | elf |
          elves.push(([*elf.split('-')[0]..elf.split('-')[1]]))
        end

        if elves[0].any?{|x| elves[1].include?(x)}
          kindaOverlaps += 1
        end
      end

      return kindaOverlaps      
    end
  end
end
