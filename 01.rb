module Day01
  class << self
    def part_one(input)
      totals = [0]

      input.each do | line |
        if line.empty?
          totals.prepend(0)
        else
          totals[0] += line.to_i
        end
      end

      return totals.max
    end

    def part_two(input)
      totals = [0]

      input.each do | line |
        if line.empty?
          totals.prepend(0)
        else
          totals[0] += line.to_i
        end
      end

      return totals.max(3).sum
    end
  end
end
