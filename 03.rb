module Day03
  class << self

    ITEM_KEY = [
      'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
      'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
    ]

    def part_one(input)
      total = 0
      input.each do | rucksack |
        comp1, comp2 = rucksack.partition(/.{#{rucksack.size/2}}/)[1,2]
        common = []
        comp1.split('').each do | item |
          if comp2.include?(item) && !common.include?(item)
            common.push(item)
            total += ITEM_KEY.index(item) + 1
          end
        end
      end
      return total
    end

    def part_two(input)
      total = 0
      input.each_slice(3) do | group |
        badge_item = group[0].split('') & group[1].split('') & group[2].split('')
        total += ITEM_KEY.index(badge_item[0].to_s) + 1
      end
      return total
    end

  end
end
