module Day02
  class << self
    WIN = 6
    TIE = 3
    LOSE = 0

    WIN_EXTRA_PTS = {
      rock: 1,
      paper: 2,
      scissors: 3
    }

    ORDER = %i[rock paper scissors]

    def play(move1, move2)
      return TIE if move1 == move2
      idx = ORDER.index(move1)
      return move2 == ORDER[(idx + 1) % ORDER.length] ? WIN : LOSE
    end

    def score(move1, move2)
      WIN_EXTRA_PTS[move2] + play(move1, move2)
    end

    def winning_move(move1, me)
      idx = ORDER.index(move1)
      case me
      when 'X'
        ORDER[idx - 1]
      when 'Y'
        move1
      when 'Z'
        ORDER[(idx + 1) % ORDER.length]
      end
    end

    def part_one(input)
      total = 0

      input.each do | round |
        opponent, me = round.split(' ')
        move1 = ORDER[opponent.ord - 'A'.ord]
        move2 = ORDER[me.ord - 'X'.ord]
        total += score(move1, move2)
      end

      return total
    end

    def part_two(input)
      total = 0

      input.each do | round |
        opponent, me = round.split(' ')
        move1 = ORDER[opponent.ord - 'A'.ord]
        move2 = ORDER[me.ord - 'X'.ord]
        total += score(move1, winning_move(move1, me))
      end
      
      return total
    end
  end
end
